import datetime
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine

connect_string = 'mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8'.format("CPMWMli5c3", "4LcKkgc4qo", "remotemysql.com",
                                                                      "3306", "CPMWMli5c3")
engine = create_engine(connect_string)
Base = automap_base()
Base.prepare(engine, reflect=True)
session = Session(engine)


def menu():
    print("----------- MAIN MENU -----------")
    print("1. Donar d'alta un usuari")
    print("2. Modificar quota de soci")
    print("3. Eliminar un soci")
    print("4. Llistar un soci")
    print("5. Exit")
    print("-----------------------------------")


def listaSocios():
    print(":: LLISTAT DE SOCIS ::")
    for socio in session.query(Base.classes.SOCIO).all():
        print("Codigo de socio: ", socio.COD_SOC,
              " Nombre: ", socio.NOMBRE,
              " Apellidos: ", socio.APELLIDOS,
              " Direccion: ", socio.DIRECCION)

    print('')


def editarSocio():
    listaSocios()
    print(":: CANVI DE QUOTA DE SOCI ::\n")
    codSoc = input("Quin usuari es vol modificar? (Introdueix COD_SOC)")
    socio = session.query(Base.classes.SOCIO).get(codSoc)
    print("Codigo de socio: " + str(
        socio.COD_SOC) + " Nombre: " + socio.NOMBRE + " Apellidos: " + socio.APELLIDOS + " Direccion: " + socio.DIRECCION);
    conf = input("Es vol canviar la cuota?(S/N)")
    if conf.lower() == "s":
        socio.CUOTA = input("Introdueix la quantitat de la cuota: ")
        session.commit()
        print("record modyfied")


def borrarSocio():
    listaSocios()
    print(":: ELIMINAR SOCI ::")
    codSoc = input("Quin usuari es vol eliminar? (Introdueix COD_SOC)")
    socio = session.query(Base.classes.SOCIO).get(codSoc)
    print("Codigo de socio: " + str(
        socio.COD_SOC) + " Nombre: " + socio.NOMBRE + " Apellidos: " + socio.APELLIDOS + " Direccion: " + socio.DIRECCION)
    conf = input("Estas seguro que el vols eliminar?(S/N)")

    if conf.lower() == "s":
        session.delete(socio)
        session.commit()
        print("record deletied")


def añadirSocio():
    print(":: DONAR D'ALTA UN USUARI ::")
    Socio = Base.classes.SOCIO
    nom = input("Nom: ")
    cognom = input("Cognom: ")
    direccio = input("Direccio: ")
    telefon = input("Telefon: ")
    poblacio = input("Poblacio: ")
    cp = input("CP: ")
    provincia = input("Provincia: ")
    pais = input("Pais: ")
    edat = int(input("Edat: "))
    cuota = int(input("Quota: "))

    session.add(Socio(COD_SOC=idPartner() + 1
                      , NOMBRE=nom
                      , APELLIDOS=cognom
                      , DIRECCION=direccio
                      , TELEFONO=telefon
                      , POBLACION=poblacio
                      , CP=cp
                      , PROVINCIA=provincia
                      , PAIS=pais
                      , EDAD=edat
                      , CUOTA=cuota
                      , FECHAALTA=datetime.date.today()
                      ))
    session.commit()
    print("record inserted")


def idPartner():
    obj = session.query(Base.classes.SOCIO).order_by(Base.classes.SOCIO.COD_SOC.desc()).first()
    return obj.COD_SOC


while True:
    menu()
    opcion = int(input("Selecciona una opcio: "))
    if opcion == 1:
        añadirSocio()
        print()
    elif opcion == 2:
        editarSocio()
        print()
    elif opcion == 3:
        borrarSocio()
        print()
    elif opcion == 4:
        listaSocios()
    elif opcion == 5:
        break
    else:
        print("Introdueix un numero entre 1 i 5")
